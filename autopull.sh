#!/bin/sh
#https://stackoverflow.com/questions/3258243/check-if-pull-needed-in-git

#cd ~/git-auto-update-test/demo-1

#Check the internet connection
case "$(curl -s --max-time 2 -I http://google.com | sed 's/^[^ ]*  *\([0-9]\).*/\1/; 1q')" in
  [23]) echo "HTTP connectivity is up"

git remote update

UPSTREAM=${1:-'@{u}'}
LOCAL=$(git rev-parse @)
REMOTE=$(git rev-parse "$UPSTREAM")
BASE=$(git merge-base @ "$UPSTREAM")

if [ $LOCAL = $REMOTE ]; then
    echo "Up-to-date"
elif [ $LOCAL = $BASE ]; then
    echo "Need to pull"
    git pull
    /var/www/html/slideshow/system/mainscript.sh
    /var/www/html/slideshow/system/sendmail.sh
    #sudo shutdown -r +1
    startx /home/pi/.xsession
elif [ $REMOTE = $BASE ]; then
    echo "Need to push"
    echo "Subject:Quboo-Box: " $ipaddress " git - Need to push" | ssmtp technik@quboo.ch
else
    echo "Diverged"
    echo "Subject:Quboo-Box: " $ipaddress " git - Diverged" | ssmtp technik@quboo.ch
fi;;
  5) echo "The web proxy won't let us through";;
  *) echo "The network is down or very slow";;
esac
